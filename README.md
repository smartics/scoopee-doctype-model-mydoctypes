# Sample Doctypes Project


## Overview

This project is the result of running the Doctype Maven Plugin to create a
[doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw) with sample files
for the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).

The sample add-on provides a space, one doctype, and a type doctype.

## Fork me!
Feel free to fork this project to adjust the model according to your project
requirements.

We recommend you create this project by the use of the create model mojo. Just
follow the [step-by-step tutorial](https://www.smartics.eu/confluence/x/sQIbBg).

The source files of this project are licensed under
[Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)
