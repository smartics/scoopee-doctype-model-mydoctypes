AJS.bind("blueprint.wizard-register.ready", function () {
  Confluence.Blueprint.setWizard(
    'de.scoopee.projectdoc.scoopee-doctype-addon-mydoctypes:create-doctype-template-minutes',
    function(wizard) {
      wizard.on('pre-render.page1Id', function(e, state) {
        state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
      });

      wizard.on("post-render.page1Id", function(e, state) {
        $('#day-date').datePicker({
          overrideBrowserDefault: true,
          dateFormat : "yy-mm-dd"
        });

        $('#day-date').attr('placeholder', AJS.I18n.getText('projectdoc.doctype.minutes.wizard.placeholder')) ;

        PROJECTDOC.postrenderWizard(e, state);
      });

      wizard.on('submit.page1Id', function(e, state) {
        return PROJECTDOC.validateStandardForm(e, state);
      });

      wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
    });
});
